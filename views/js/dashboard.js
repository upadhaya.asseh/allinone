import { showAlert } from "./alert.js"

// logging out
const logout = async () => {
    try {
        const res = await axios ({
            method: 'GET',
            url:'http://localhost:4001/api/v1/users/logout',
        })
        if (res.data.status === 'success') {
            location.reload(true)
        }
    } catch (err) {
        showAlert('error', 'Error logging out! Try again.')
    }
}

var obj
if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))
}else
{
    obj = JSON.parse('{}')
}

var el = document.querySelector('.nav.nav--user')
if(obj._id) {
    el.innerHTML = 
    '<a id = "logout" class="nav__el">Log out</a>'
    //  <a href="/me" class="nav__el"><img scr="../img/users/' + 
    // obj.photo +
    // '" alt="Photo of ${user.name}" class="nav__user-img" /><span>'+
    // ogj.name +  
    // '</span> </a>'

    // //  el.innerHTML = 
    //     ' <div class = "login_b"> <a class = "login_button" id="logout">logout</a>'  
   
        var doc = document.querySelector('#logout')

    doc.addEventListener('click', (e) => logout())
}
else
{
    // el.innerHTML = 
    //     ' <div class = "login_b"><a class = "login_button" href="/login">Login</a>'  
    el.innerHTML =  
    '<a class="nav__el nav__el--cta" href="/login">Log in</a> <a class="nav__el nav__el--cta" href="/signup">Sign up</a>'
}
//  <a herf="/me" class="nav__el"><img scr="../img/users/'+obj.photo + 
//     'alt ="Phot0 of ${user.name}" class="nav__user-img" /><span>' + obj.name + '</span> </a>