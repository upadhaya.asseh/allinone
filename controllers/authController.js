const User= require('./../model/userModels')
const jwt = require('jsonwebtoken')
const AppError = require('./../utils/appError')


const signToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

const createSendToken =(user, statusCode, res) => {
const token = signToken(user._id)
const cookieOptions = {
  expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
  httpOnly: true
};

  res.cookie('jwt', token, cookieOptions);

  res.status(statusCode).json ({
    status: 'success',
        token,
        data:{
            user,
        },

  });
};

exports.signup = async (req, res, next) => {
  try {
    const newUser = await User.create(req.body);
    createSendToken(newUser, 201, res)
    //  const token = signToken(newUser._id)

  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body
    console.log("hello login1");

    // 1)check if email and password exist
    if (!email || !password) {
      console.log("hello login2");

      return next(new AppError("please provide as email and password!", 400));
    }
    // 2) check if user exists && password is correct
    const user = await User.findOne({ email }).select("+password");
    console.log(email);

    // console.log(password)
    if (!user || !(await user.correctPassword(password, user.password))) {
      return next(new AppError("Incorrect email or password", 401));
    }
    // 3)if everything ok, send token to client
    createSendToken(user, 200, res);
    // const token = signToken(user._id)
    // // console.log("hello login5")

    // res.status(200).json({
    //     status: 'success',
    //     token,
    //     data : {
    //       user
    //     }
    // }
    // )
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

exports.logout = (req, res) => {
  res.cookie('token', '', {
    expires: new Date(Date.now() + 10 * 100),
    httpOnly: true,
  })
  res.status(200).json({status: 'success'})
}